﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace mapz4
{
    public interface IThemeFactory
    {
        IObstaclesTexture CreateObstaclesObject();

        IBackgroundTexture CreateBackgroundObject();
    }

    class AquaThemeFactory : IThemeFactory
    {
        public IObstaclesTexture CreateObstaclesObject()
        {
            return new AquaObstaclesTexture();
        }

        public IBackgroundTexture CreateBackgroundObject()
        {
            return new AquaBackgroundTexture();
        }
    }

    class FireThemeFactory : IThemeFactory
    {
        public IObstaclesTexture CreateObstaclesObject()
        {
            return new FireObstaclesTexture();
        }

        public IBackgroundTexture CreateBackgroundObject()
        {
            return new FireBackgroundTexture();
        }
    }

    public interface IObstaclesTexture
    {
        string GetObstaclesTexture();
    }

    class AquaObstaclesTexture : IObstaclesTexture
    {
        public string GetObstaclesTexture()
        {
            return "C:\\Users\\Ноутбук\\Desktop\\mapz4\\lab4\\mapz4\\images\\bomb.png";
        }
    }

    class FireObstaclesTexture : IObstaclesTexture
    {
        public string GetObstaclesTexture()
        {
            return "C:\\Users\\Ноутбук\\Desktop\\mapz4\\lab4\\mapz4\\images\\fire.png";
        }
    }


    public interface IBackgroundTexture
    {
        string GetBackgroundTexture();
    }

    class AquaBackgroundTexture : IBackgroundTexture
    {
        public string GetBackgroundTexture()
        {
            return "C:\\Users\\Ноутбук\\Desktop\\mapz4\\lab4\\mapz4\\images\\aquabackground.jpg";
        }
    }

    class FireBackgroundTexture : IBackgroundTexture
    {
        public string GetBackgroundTexture()
        {
            return "C:\\Users\\Ноутбук\\Desktop\\mapz4\\lab4\\mapz4\\images\\firebackground.jpg";
        }
    }

    class GameThemeSelect
    {
        private IThemeFactory gameThemeFactory;

        public GameThemeSelect(IThemeFactory gameThemeFactory)
        {
            this.gameThemeFactory = gameThemeFactory;
        }

        public IObstaclesTexture GetObstaclesStyleObject()
        {
            return gameThemeFactory.CreateObstaclesObject();
        }

        public IBackgroundTexture GetBackgroundStyleObject()
        {
            return gameThemeFactory.CreateBackgroundObject();
        }
    }
}
