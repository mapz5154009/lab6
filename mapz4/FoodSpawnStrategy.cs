﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;

namespace mapz4
{
    public interface IFoodSpawnStrategy
    {
        List<Point> GetFoodPoints();
    }
    public class RandomFoodSpawnStrategy : IFoodSpawnStrategy
    {
        private readonly Random random;
        private const int segmentSize = 10;

        public RandomFoodSpawnStrategy()
        {
            random = new Random();
        }

        public List<Point> GetFoodPoints()
        {
            List<Point> points = new List<Point>();
            int x, y;
            Canvas singletonCanvas = SingletonCanvas.GetInstance();

            while (points.Count < 5)
            {
                x = random.Next(0, (int)(singletonCanvas.ActualWidth / (segmentSize * 2)));
                y = random.Next(0, (int)(singletonCanvas.ActualHeight / (segmentSize * 2)));

                Point newPoint = new Point(x, y);
                if (points.TrueForAll(f => f.X != x || f.Y != y))
                    points.Add(newPoint);
            }

            return points;
        }
    }

    public class RowFoodSpawnStrategy : IFoodSpawnStrategy
    {
        private readonly Random random;
        private const int segmentSize = 10;

        public RowFoodSpawnStrategy()
        {
            random = new Random();
        }

        public List<Point> GetFoodPoints()
        {
            List<Point> points = new List<Point>();
            int x, y;
            Canvas singletonCanvas = SingletonCanvas.GetInstance();

            x = random.Next(0, (int)(singletonCanvas.ActualWidth / (segmentSize * 2) - 9));
            y = random.Next(0, (int)(singletonCanvas.ActualHeight / (segmentSize * 2)));

            while (points.Count < 5)
            {
                Point newPoint = new Point(x, y);
                points.Add(newPoint);
                x += 2;
            }

            return points;
        }
    }

}
