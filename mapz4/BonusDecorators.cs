﻿using System.Windows;
using System;
using System.Linq;

namespace mapz4
{
    public interface IBonus
    {
        void ApplyChanges(Snake snake);
    }

    class Bonus : IBonus
    {
        public int lengthIncrease;

        public Bonus()
        {
            lengthIncrease = 5;
        }

        public void ApplyChanges(Snake snake) 
        {
            // Збільшити довжину змійки на lengthIncrease
            for (int i = 0; i<lengthIncrease; i++)
            {
                snake.Grow();
            }
        }
    }

    abstract class Decorator : IBonus
    {
        protected IBonus bonus;

        public Decorator(IBonus bonus) 
        {
            this.bonus = bonus;
        }

        public virtual void ApplyChanges(Snake snake)
        {
            bonus.ApplyChanges(snake);
        }
    }

    class AdditionalLifeDecorator : Decorator
    { 
        public AdditionalLifeDecorator(IBonus bonus) : base(bonus) { }

        public override void ApplyChanges(Snake snake)
        {
            // Дати змійці додаткове життя
            snake.AdditionalLife = true;
        }
    }

    class RandomTeleportDecorator : Decorator
    {
        public RandomTeleportDecorator(IBonus bonus) : base(bonus) { }

        public override void ApplyChanges(Snake snake)
        {
            // Телепортує змійку на рандомні координати на карті
            base.ApplyChanges(snake);
            Random random = new Random();
            int x = (int)(snake.Body[0].X - random.Next(0, 780 / 20));
            int y = (int)(snake.Body[0].Y - random.Next(0, 380 / 20));

            for(int i=0; i<snake.Body.Count(); i++) 
            {
                Point p = snake.Body[i];
                p.X += x;
                p.Y += y;
                snake.Body[i] = p;
            }
        }
    }
}
