﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace mapz4
{
    public class Snake
    {
        public string Name { get; set; }
        public int Length { get; private set; }
        public List<Point> Body { get; set; }
        public Direction Direction { get; set; }
        public SolidColorBrush Color { get; set; }
        public bool AdditionalLife { get; set; }
        public bool Armor { get; set; }
        public int Speed { get; set; }

        public Snake(string name, Point body, SolidColorBrush color)
        {
            Name = name;
            Body = new List<Point> { body };
            Length = Body.Count;
            Direction = Direction.Right;
            Color = color;
            AdditionalLife = false;
            Armor = false;
            Speed = 20;
        }

        public void Grow()
        {
            Point lastSegment = Body[Body.Count - 1];

            switch (Direction)
            {
                case Direction.Up:
                    Body.Add(new Point(lastSegment.X, lastSegment.Y + 1));
                    break;
                case Direction.Down:
                    Body.Add(new Point(lastSegment.X, lastSegment.Y - 1));
                    break;
                case Direction.Left:
                    Body.Add(new Point(lastSegment.X + 1, lastSegment.Y));
                    break;
                case Direction.Right:
                    Body.Add(new Point(lastSegment.X - 1, lastSegment.Y));
                    break;
            }

            Length = Body.Count;
        }

        public void ChangeDirection(Direction newDirection)
        {
            if (Direction != GetOppositeDirection(newDirection))
            {
                Direction = newDirection;
                MessageBox.Show($"Змінено напрямок руху на {newDirection}!");
            }
        }

        private Direction GetOppositeDirection(Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    return Direction.Down;
                case Direction.Down:
                    return Direction.Up;
                case Direction.Left:
                    return Direction.Right;
                case Direction.Right:
                    return Direction.Left;
            }

            return Direction.Up;
        }
    }

    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }
}
