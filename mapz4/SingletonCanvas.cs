﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace mapz4
{
    class SingletonCanvas
    {
        private static Canvas instance;

        private SingletonCanvas() {}

        public static Canvas GetInstance()
        {
            if (instance == null)
            {
                instance = new Canvas(); 
            }
            
            return instance;
        }
    }   
}
