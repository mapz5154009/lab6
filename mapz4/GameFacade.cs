﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Shapes;

namespace mapz4
{
    public interface IGameFacade
    {
        void InitializeCanvas(Canvas gameCanvas);
        void CreateGame(string gameMode, IThemeFactory themeFactory, bool obs, List<Point> foodPoints);
        void ShowGameMode();
        bool AddSnake(string name);
        void ApplyBonus(IBonus bonus);
        List<Snake> GetSnakeList();
    }

    public class GameFacadeProxy : IGameFacade
    {
        private readonly GameFacade facade;
        private const int maxSnakesAllowed = 20;

        public GameFacadeProxy(GameFacade facade)
        {
            this.facade = facade;
        }

        public void InitializeCanvas(Canvas gameCanvas)
        {
            facade.InitializeCanvas(gameCanvas);
        }

        public void CreateGame(string gameMode, IThemeFactory themeFactory, bool obs, List<Point> foodPoints)
        {
            facade.CreateGame(gameMode, themeFactory, obs, foodPoints);
        }

        public bool AddSnake(string name)
        {
            if (facade.snakes.Count < maxSnakesAllowed)
            {
                facade.AddSnake(name);
                return true;
            }
            else
            {
                MessageBox.Show($"Максимальна кількість змійок {maxSnakesAllowed}!");
                return false;
            }
        }

        public void ShowGameMode()
        {
            facade.ShowGameMode();
        }

        public void ApplyBonus(IBonus bonus)
        {
            facade.ApplyBonus(bonus);
        }

        public List<Snake> GetSnakeList()
        {
            return facade.GetSnakeList();
        }
    }

    public class GameFacade : IGameFacade
    {
        private Canvas singletonCanvas;
        private const int segmentSize = 10;
        private Random random = new Random();
        public List<Snake> snakes { get; private set; }
        private List<Point> obstacles;
        private const int obstaclesPercentage = 9;
        private List<Food> foodPrototypes;
        private List<Food> food;
        private string gameMode;

        public GameFacade()
        {
            snakes = new List<Snake>();
            obstacles = new List<Point>();
            foodPrototypes = new List<Food>() { new Apple(), new GoldenApple(), new WaterMelon() };
            food = new List<Food>();
        }

        public void InitializeCanvas(Canvas gameCanvas)
        {
            singletonCanvas = SingletonCanvas.GetInstance();
            gameCanvas.Children.Add(singletonCanvas);

            singletonCanvas.Height = gameCanvas.Height - 20;
            singletonCanvas.Width = gameCanvas.Width - 20;
            singletonCanvas.Margin = new Thickness(10);
            singletonCanvas.Background = Brushes.Purple;
        }

        public void CreateGame(string gameMode, IThemeFactory themeFactory, bool obs, List<Point> foodPoints)
        {
            singletonCanvas.Children.Clear();
            snakes.Clear();
            obstacles.Clear();
            food.Clear();

            this.gameMode = gameMode;

            GameThemeSelect gameTheme = new GameThemeSelect(themeFactory);

            DrawTextures(singletonCanvas, gameTheme, obs, foodPoints);
        }

        private void DrawTextures(Canvas canvas, GameThemeSelect gameTheme, bool obs, List<Point> foodPoints)
        {
            ImageBrush imageBrush = new ImageBrush();
            BitmapImage bitmap = new BitmapImage(new Uri(gameTheme.GetBackgroundStyleObject().GetBackgroundTexture()));
            imageBrush.ImageSource = bitmap;

            canvas.Background = imageBrush;

            GenerateFood(canvas, foodPoints);

            if (obs)
            {
                GenerateObstacles(canvas, gameTheme.GetObstaclesStyleObject().GetObstaclesTexture(), foodPoints);
            }

        }

        private void GenerateObstacles(Canvas canvas, string path, List<Point> foodPoints)
        {
            Random random = new Random();
            int x, y;

            for (int i = 0; i < canvas.ActualWidth / (segmentSize * 2); i++)
            {
                for (int j = 0; j < canvas.ActualHeight / (segmentSize * 2); j++)
                {
                    x = random.Next(0, obstaclesPercentage);
                    y = random.Next(0, obstaclesPercentage);
                    Point newPoint = new Point(i, j);
                    if (x == 0 && y == 0 && !foodPoints.Contains(newPoint))
                    {
                        obstacles.Add(newPoint);

                        Image image = new Image();
                        BitmapImage bitmap = new BitmapImage(new Uri(path));
                        image.Source = bitmap;

                        image.Width = (segmentSize * 2);
                        image.Height = (segmentSize * 2);

                        canvas.Children.Add(image);

                        Canvas.SetLeft(image, i * (segmentSize * 2));
                        Canvas.SetTop(image, j * (segmentSize * 2));
                    }
                }
            }
        }

        private void GenerateFood(Canvas canvas, List<Point> foodPoints)
        {
            Random random = new Random();

            foreach (var newPoint in foodPoints)
            {
                int a = random.Next(0, foodPrototypes.Count);
                Food clonedFood = foodPrototypes[a].Clone();
                clonedFood.point = newPoint;
                food.Add(clonedFood);

                Image image = new Image();
                BitmapImage bitmap = new BitmapImage(new Uri(clonedFood.textureUri));
                image.Source = bitmap;

                image.Width = segmentSize * 2;
                image.Height = segmentSize * 2;

                canvas.Children.Add(image);

                Canvas.SetLeft(image, newPoint.X * (segmentSize*2));
                Canvas.SetTop(image, newPoint.Y * (segmentSize * 2));
            }        
        }

        public void ShowGameMode()
        {
            MessageBox.Show(gameMode);
        }
        public bool AddSnake(string name)
        {
            int xmax = (int)singletonCanvas.ActualWidth / segmentSize;
            int ymax = (int)singletonCanvas.ActualHeight / segmentSize;
            int x = random.Next(0, xmax);
            int y = random.Next(0, ymax);
            Point point = new Point(x, y);

            byte red = (byte)random.Next(0, 256);
            byte green = (byte)random.Next(0, 256);
            byte blue = (byte)random.Next(0, 256);
            Color randomColor = Color.FromArgb(255, red, green, blue);
            SolidColorBrush color = new SolidColorBrush(randomColor);

            Snake snake = new Snake(name, point, color);

            snakes.Add(snake);

            DrawRectangle(singletonCanvas);

            return true;
        }

        private void DrawRectangle(Canvas canvas)
        {
            foreach (var snake in snakes)
            {
                Rectangle rect = new Rectangle();
                rect.Width = segmentSize;
                rect.Height = segmentSize;
                rect.Fill = snake.Color;

                canvas.Children.Add(rect);

                Canvas.SetLeft(rect, segmentSize * snake.Body[0].X);
                Canvas.SetTop(rect, segmentSize * snake.Body[0].Y);
            }
        }

        public void ApplyBonus(IBonus bonus) 
        {
            Random random = new Random();
            int i = random.Next(0, snakes.Count());

            bonus.ApplyChanges(snakes[i]);
        }

        public List<Snake> GetSnakeList()
        {
            return snakes;
        }
    }
}
